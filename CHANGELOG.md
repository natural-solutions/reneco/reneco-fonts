# [1.8.0](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.7.3...v1.8.0) (2025-02-20)


### Features

* duplicate NG icons ([f791c17](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/f791c17cb799c81d14540cde628feb25ac375578))

## [1.7.3](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.7.2...v1.7.3) (2024-11-04)


### Bug Fixes

* trackng icons ([f0c7b78](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/f0c7b78d5e0f7969ac09b522b410c73f7aef9ebc))

## [1.7.2](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.7.1...v1.7.2) (2023-07-21)


### Bug Fixes

* add mui variables ([f2c5401](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/f2c5401f4bb221eb61973fa8c458d18a066b9275))

## [1.7.1](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.7.0...v1.7.1) (2023-05-25)


### Bug Fixes

* theme dark ([dc53020](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/dc53020b0a3776cc9f5e294e27d7915be4c91403))

# [1.7.0](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.6.3...v1.7.0) (2023-04-03)


### Features

* add file-preview icon ([7c7c5fd](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/7c7c5fde2597c9b4a4fb83c7089759ee12403870))

## [1.6.3](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.6.2...v1.6.3) (2023-02-01)


### Bug Fixes

* test release 2 ([c34bfa4](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/c34bfa46b1de2add7285cd87f2ce10beb9f4399a))

## [1.6.2](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.6.1...v1.6.2) (2023-02-01)


### Bug Fixes

* replace NPM_TOKEN for NPM_RENECO_PUBLISH ([847d570](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/847d570552f5494db1d5729796f8c9ec02b022a8))

## [1.6.1](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.6.0...v1.6.1) (2023-02-01)


### Bug Fixes

* test release with npm publish ([5de5a92](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/5de5a923cde09353435ecc4a324e476a42e40f71))

# [1.6.0](https://gitlab.com/natural-solutions/reneco/reneco-fonts/compare/v1.5.7...v1.6.0) (2023-01-31)


### Features

* ajout des variables mui ([65d4c24](https://gitlab.com/natural-solutions/reneco/reneco-fonts/commit/65d4c247317443e1c2f5562099c3e637aaf91e0a))

## [1.5.7](https://gitlab.com/natural-solutions/reneco-fonts/compare/v1.5.6...v1.5.7) (2022-12-14)


### Bug Fixes

* call npm ([992788d](https://gitlab.com/natural-solutions/reneco-fonts/commit/992788d2732484729ca7c4f6e33354358cdde206))

## [1.5.6](https://gitlab.com/natural-solutions/reneco-fonts/compare/v1.5.5...v1.5.6) (2022-07-13)


### Bug Fixes

* Add color version on all Reneco logos ([b3dcfd0](https://gitlab.com/natural-solutions/reneco-fonts/commit/b3dcfd00e3b596909b905e89b9b788ce485709a3))

## [1.5.5](https://gitlab.com/natural-solutions/reneco-fonts/compare/v1.5.4...v1.5.5) (2022-07-13)


### Bug Fixes

* add recursive ([d30580b](https://gitlab.com/natural-solutions/reneco-fonts/commit/d30580bf1500bbc58c703bbff2690997bdbb99ec))
* added demo files to public folder on pages deploy stage ([4c5ccfe](https://gitlab.com/natural-solutions/reneco-fonts/commit/4c5ccfe5a0cfda11e2ebe655163e1f8b5ad73c6f))
* copy fonts folder on public folder pages ([191a05d](https://gitlab.com/natural-solutions/reneco-fonts/commit/191a05d905bb3deda236e5ce5c44953116117fa6))
